import React from 'react'
import '../static.neris-assets.com/css/main-core--6374ea.css'
import '../static.neris-assets.com/css/large--3e300a.css'
import Layout from '../components/Layout'
import './index.css'
const index = () => {
 
    return (
        <Layout>
            <main class="q-hm">

                <div class="hero">
                    <div class="text-wrapper">
                        <h1 className='head'>“It’s so incredible to finally be understood.”</h1>
                        <p class="subtitle">Take our Personality Test and get a “freakishly accurate” description of who you are and why you do things the way you do.</p>
                    </div>
                    <div class="action-wrapper">
                        <a href="#" class="btn btn-action" style={{borderRadius:'35px'}} dusk="quiz-button">Take the Test <span class="fas fa-arrow-right"></span></a>
                    </div>
                </div>

                <div class="scene">
                    <img src="https://static.neris-assets.com/images/homepage/welcome.svg" alt='loading...'/>
                    <svg viewBox="0 0 1920 476" preserveAspectRatio="none"><polygon class="c1" points="1920 476 1810 403 1158 259 578 391 420 303 219 442 0 476 0 0 1920 0 1920 476"></polygon></svg>
                </div>

                <div class="stats-wrapper">
                    <div class="stats">
                        <div class="counter">387,207,613</div>
                        <div class="text">Tests taken so far</div>
                    </div>
                    <p class="subtitle">Curious how accurate we are about you?<br /> <a class="with-border" href="free-personality-test.html">Take the test</a> and find out.</p>
                </div>

                <div class="reviews">
                    <div class="review diplomat">
                        <div class="f16 f16-quote"></div>
                        <div class="text">“I was honestly shocked how accurate it was. I teared up a bit because it was like there was a person looking inside my mind and telling me what they saw.”</div>
                        <img src="https://static.neris-assets.com/images/personality-types/avatars/faces/infj-advocate-s3-v1-female.svg" alt='loading...' />
                        <div class="name">Emma – “The Advocate”</div>
                    </div>
                    <div class="review explorer">
                        <div class="f16 f16-quote"></div>
                        <div class="text">“I can’t believe how accurate this was... Word for word. I never felt so understood. I thought I was the only one of my kind.”</div>
                        <img src="https://static.neris-assets.com/images/personality-types/avatars/faces/isfp-adventurer-s3-v1-female.svg" alt='loading...' />
                        <div class="name">Ashley – “The Adventurer”</div>
                    </div>
                    <div class="review analyst">
                        <div class="f16 f16-quote"></div>
                        <div class="text">“It is undeniably eerie how the description and traits nailed me so thoroughly. Wish I had known about my personality type 20 years ago, that would have saved me much grief.”</div>
                        <img src="https://static.neris-assets.com/images/personality-types/avatars/faces/intj-architect-s3-v1-male.svg" alt='loading...' />
                        <div class="name">Augustine – “The Architect”</div>
                    </div>
                </div>

                <div class="middle-cta">
                    <a href="#" style={{borderRadius:'35px'}} class="btn btn-action btn-lg">Take the Test <span class="fas fa-arrow-right"></span></a>
                </div>

                <div class="middle-cover">
                    <div class="vertical-line"></div>
                    <svg viewBox="0 0 1920 190" preserveAspectRatio="none"><polygon class="c1" points="1920 313 0 313 0 181 465 0 1127 125 1513 62 1920 182 1920 313"></polygon></svg>
                </div>

                <div class="other-actions">
                    <h2>What else can you do here?</h2>
                    <div class="horizontal-line"></div>
                    <div class="actions">
                        <div class="action first">
                            <div class="info">
                                <div class="number">01</div>
                                <h3>Understand others</h3>
                                <p>In our free type descriptions you’ll learn what really drives, inspires, and worries different personality types, helping you build more meaningful relationships.</p>
                                <a class="btn" href="#" style={{borderRadius:'35px'}}>Read about Types</a>
                            </div>
                            <div class="image-wrapper">
                                <img src="https://static.neris-assets.com/images/homepage/community.svg" alt='loading...'/>
                            </div>
                        </div>
                        <div class="action second">
                            <div class="image-wrapper">
                                <img src="https://static.neris-assets.com/images/homepage/academy.svg" alt='loading...'/>
                            </div>
                            <div class="info">
                                <div class="number">02</div>
                                <h3>Get a roadmap for success</h3>
                                <p>Our premium profiles are for those who want to dive deeper into their personality and learn how to grow and better navigate the world around them.</p>
                                <a class="btn" href="#" style={{borderRadius:'35px'}}>Get Your Premium Profile</a>
                            </div>
                        </div>
                        <div class="action third">
                            <div class="info">
                                <div class="number">03</div>
                                <h3>Explore our toolkits</h3>
                                <p>We’ve developed many tools and assessments to help you explore how your personality traits affect various aspects of your life – your confidence, perfectionism, burnout, and more.</p>
                                <a class="btn" href="#" style={{borderRadius:'35px'}}>See Tools & Assessments</a>
                            </div>
                            <div class="image-wrapper">
                                <img src="https://static.neris-assets.com/images/homepage/personality_types.svg" alt='loading...'/>
                            </div>
                        </div>
                    </div>
                </div>

                <svg class="trust-background-1" viewBox="0 0 1920 187" preserveAspectRatio="none"><polygon class="c1" points="1920,187 0,187 0,181 82,172 458,31 778,110 1354,0 1920,182"></polygon></svg>

                <div class="why-trust">
                    <h2>Why trust us?</h2>
                    <div class="horizontal-line"></div>
                    <div class="trust-wrapper">
                        <div class="point">
                            <div class="icon-wrapper">
                                <img class="icon" src="https://static.neris-assets.com/images/homepage/research_model.svg" alt='loading...'/>
                            </div>
                            <div class="info">
                                <h3>Modern and Reliable Framework</h3>
                                <div class="description">Our <a href="#" title="Our framework">personality model</a> incorporates the latest advances in psychometric research, combining time-tested concepts with robust and highly accurate testing techniques.</div>
                            </div>
                        </div>
                        <div class="point">
                            <div class="icon-wrapper">
                                <img class="icon" src="https://static.neris-assets.com/images/homepage/studies.svg" alt='loading...'/>
                            </div>
                            <div class="info">
                                <h3>Hundreds of Pioneering Studies</h3>
                                <div class="description">Dig into <a href="#" title="Our studies">our studies</a> on personality types and their impact on our lives – <a href="country-profiles/global/world.html" title="Country profiles">geographical distribution</a>, social attitudes, relationships, and much more.</div>
                            </div>
                        </div>
                        <div class="point">
                            <div class="icon-wrapper">
                                <img class="icon" src="https://static.neris-assets.com/images/homepage/languages.svg" alt='loading...'/>
                            </div>
                            <div class="info">
                                <h3>Available in a Number of Languages</h3>
                                <div class="description">At <a href="#">37 languages</a>, our test is the most translated major personality test on the internet. Speaking French, Spanish or Lithuanian? Take the test in your language!</div>
                            </div>
                        </div>
                    </div>
                    <div class="action-row">
                        <a href="#" class="btn btn-action btn-lg" style={{borderRadius:'35px'}}>Take the Test <span class="fas fa-arrow-right"></span></a>
                    </div>
                </div>

                <svg class="trust-background-2" viewBox="0 0 1920 187" preserveAspectRatio="none"><path class="c1" d="M0 0h1920v64l-238-20-448 143L396 45 0 103z"></path></svg>
            </main>
            {/*------------Footer----------------  */}
            <footer id="footer" class="">

                <div className="copyright">
                    ©2011-2021 NERIS Analytics Limited
                </div>
                <nav className="links">
                    <a className="with-border" href="#">Contact</a>
                    <a className="with-border" href="#">Testimonials</a>
                    <a className="with-border" href="#">Help Us Evolve!</a>
                    <a className="with-border" href="#">Terms & Conditions</a>
                    <a className="with-border" href="#">Privacy Policy</a>
                    <a className="with-border mindtrackers" href="#" target="_blank" rel="external">Team Assessments
             <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512"><path fill="currentColor" d="M448 241.823V464c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V112c0-26.51 21.49-48 48-48h339.976c10.691 0 16.045 12.926 8.485 20.485l-24 24a12.002 12.002 0 0 1-8.485 3.515H54a6 6 0 0 0-6 6v340a6 6 0 0 0 6 6h340a6 6 0 0 0 6-6V265.823c0-3.183 1.264-6.235 3.515-8.485l24-24c7.559-7.56 20.485-2.206 20.485 8.485zM564 0H428.015c-10.658 0-16.039 12.93-8.485 20.485l48.187 48.201-272.202 272.202c-4.686 4.686-4.686 12.284 0 16.971l22.627 22.627c4.687 4.686 12.285 4.686 16.971 0l272.201-272.201 48.201 48.192c7.513 7.513 20.485 2.235 20.485-8.485V12c0-6.627-5.373-12-12-12z"></path></svg>
                    </a>
                </nav>
                <div className="social">
                    <a className="no-formatting" href="#" target="_blank" rel="noreferrer"><span className="fab fa-facebook"></span></a>
                    <a className="no-formatting" href="#" target="_blank" rel="noreferrer"><span className="fab fa-instagram"></span></a>
                    <a className="no-formatting" href="#" target="_blank" rel="noreferrer"><span className="fab fa-twitter"></span></a>
                </div>

            </footer>

        </Layout>
    )
}

export default index
