import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import './Nav.css'
import { Navbar, Nav, Form } from 'react-bootstrap';
const NavigationBar = () => {
  return (
    <Navbar bg="light" expand="lg">
      <Navbar.Brand href="#home">
        <a href="#" className="logo ">
          <img src="https://static.neris-assets.com/images/logo.svg" alt="16Personalities" />
        </a>
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav" style={{ background: '#f8f9fa', borderRadius: '5px' }}>
        <Nav className="mr-auto nav">
          {/* ------------------Personality Types---------------------- */}
          <div className="dropdown">
            <Nav.Link href="#1" className='nav_items'>Personality Types</Nav.Link>
            <br/>
            <div className="dropdown-content">
              {/* _-----First Four--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/academy/analysts/icons/academic-path.svg' alt='loading...' width={20} height={20} style={{marginTop:'4px'}}/>
               <span className='top-button'>ANALYSTS</span>
               <p className='desc'>Intuitive (<strong>N</strong>) and Thinking (<strong>T</strong>) personality types, known for their rationality, impartiality, and intellectual excellence.</p>
                <div className='button-group'>
                <button className='button-1'>Architect</button>
                <button className='button-1'>Logician</button>
                <button className='button-1'>Commander</button>
                <button className='button-1'>Debater</button>
                </div>
              </app-nav-item>
             <br/>
               {/* _-----Second Four--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/academy/diplomats/icons/romantic-relationships.svg' alt='loading...' width={20} height={20} style={{marginTop:'4px'}}/>
               <span className='top-button'>Diplomats</span>
               <p className='desc'>Intuitive (<strong>N</strong>) and Feeling (<strong>F</strong>) personality types, known for their empathy, diplomatic skills, and passionate idealism.</p>
                <div className='button-group'>
                <button className='button-2'>Advocate</button>
                <button className='button-2'>Mediator</button>
                <button className='button-2'>Protagonist</button>
                <button className='button-2'>Campaigner</button>
                </div>
              </app-nav-item>
              <br/>
               {/* _-----Third Four--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/academy/sentinels/icons/careers.svg' alt='loading...' width={20} height={20} style={{marginTop:'4px'}}/>
               <span className='top-button'>Sentinels</span>
               <p className='desc'>Observant (<strong>S</strong>) and Judging (<strong>J</strong>) personality types, known for their practicality and focus on order, security, and stability.</p>
                <div className='button-group'>
                <button className='button-3'>Logistician</button>
                <button className='button-3'>Defender</button>
                <button className='button-3'>Executive</button>
                <button className='button-3'>Consul</button>
                </div>
              </app-nav-item>
              <br/>
               {/* _-----Forth Four--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/system/explorers-menu.svg' alt='loading...' width={20} height={20} style={{marginTop:'4px'}}/>
               <span className='top-button'>Explorers</span>
               <p className='desc'>Observant (<strong>S</strong>) and Prospecting (<strong>P</strong>) personality types, known for their spontaneity, ingenuity, and flexibility.</p>
                <div className='button-group'>
                <button className='button-4'>Virtuoso</button>
                <button className='button-4'>Adventurer</button>
                <button className='button-4'>Entrepreneur</button>
                <button className='button-4'>Entertainer</button>
                </div>
              </app-nav-item>
            </div>
          </div>

      {/* ------------------Premium Profiles---------------------- */}
          <Nav.Link href="#2" className='nav_items'>Premium Profiles</Nav.Link>

      {/* ------------------Tools & Assesments---------------------- */}
      <div className="dropdown">
            <Nav.Link href="#3" className='nav_items'>Tools & Assesments</Nav.Link>
            <br/>
            <div className="dropdown-content">
              {/* _-----First Item--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/academy/sentinels/icons/personal-growth.svg' alt='loading...' width={35} height={35} style={{marginTop:'4px'}}/>
               <span className='top-button'>Personal Growth</span>
               <p className='desc_of_Tools'>Discover and understand your strengths and weaknesses.</p>              
              </app-nav-item>
              <br/> 
              {/* _-----Second Item--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/academy/sentinels/icons/romantic-relationships.svg' alt='loading...' width={35} height={35} style={{marginTop:'4px'}}/>
               <span className='top-button'>Relationships</span>
               <p className='desc_of_Tools'>Deepen your relationships, both romantic and otherwise.</p>              
              </app-nav-item>
              <br/>
              {/* _-----Third Item--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/academy/sentinels/icons/careers.svg' alt='loading...' width={35} height={35} style={{marginTop:'4px'}}/>
               <span className='top-button'>Career</span>
               <p className='desc_of_Tools'>Kick-start your career or get better at navigating it.</p>              
              </app-nav-item>
              <br/> 
            </div>
         </div>         
         {/* ------------------Libarary---------------------- */}
      <div className="dropdown">
            <Nav.Link href="#4" className='nav_items'>Libarary</Nav.Link>
            <br/>
            <div className="dropdown-content-lib">
              {/* _-----First Item--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/academy/sentinels/icons/introduction.svg' alt='loading...' width={35} height={35} style={{marginTop:'4px'}}/>
               <span className='top-button'>Articles</span>
               <p className='desc_of_Tools'>Get tips, advice, and deep insights into personality types.</p>              
              </app-nav-item>
              <br/> 
              {/* _-----Second Item--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/academy/sentinels/icons/theory.svg' alt='loading...' width={35} height={35} style={{marginTop:'4px'}}/>
               <span className='top-button'>Theory</span>
               <p className='desc_of_Tools'>Understand the meaning and impact of personality traits.</p>              
              </app-nav-item>
              <br/>
              {/* _-----Third Item--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/system/insights.svg' alt='loading...' width={35} height={35} style={{marginTop:'4px'}}/>
               <span className='top-button'>Surveys</span>
               <p className='desc_of_Tools'>Explore and participate in hundreds of our studies.</p>              
              </app-nav-item>
              <br/> 
              {/* _-----Third Item--------- */}
              <app-nav-item>
                <img src='https://static.neris-assets.com/images/system/country-profiles-menu.svg' alt='loading...' width={35} height={35} style={{marginTop:'4px'}}/>
               <span className='top-button'>Country Profiles</span>
               <p className='desc_of_Tools'>Examine our regional and country personality profiles.</p>              
              </app-nav-item>
              <br/> 
            </div>
         </div>         
        </Nav>
        <Form inline>
          {/* <FormControl type="text" placeholder="Search" className="mr-sm-2"/> */}
          {/* <Button variant="outline-success">Search</Button> */}
          <a href="#" className="btn btn-action test-button TestButton" style={{ backgroundColor: '#51a9ab', paddingTop: '10px', paddingBottom: '10px', paddingRight: '30px', paddingLeft: '30px', borderRadius: '35px' }}>Take the Test</a>
        </Form>
      </Navbar.Collapse>
    </Navbar>
  )
}

export default NavigationBar


















