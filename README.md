<p align="center">
  <a href="https://www.gatsbyjs.com/?utm_source=starter&utm_medium=readme&utm_campaign=minimal-starter">
    <img alt="Gatsby" src="https://www.gatsbyjs.com/Gatsby-Monogram.svg" width="60" />
  </a>
</p>

## 🚀 Quick start

1.  **Download and Extract it**
2.  **Go to the project Directory**
    ```shell
    cd site-name/
    npm install or yarn
    ```

2.  **Start developing or Run Web.**
    ```shell
    cd site-name/
    gatsby develop
    ```
    Your site is now running at http://localhost:8000!

   
